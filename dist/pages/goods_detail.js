'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

var _constant = require('./../utils/constant.js');

var _tip = require('./../utils/tip.js');

var _tip2 = _interopRequireDefault(_tip);

var _api = require('./../api/api.js');

var _api2 = _interopRequireDefault(_api);

var _wxParse = require('./../plugins/wxParse/wxParse.js');

var _wxParse2 = _interopRequireDefault(_wxParse);

var _comment_list = require('./../components/comment_list.js');

var _comment_list2 = _interopRequireDefault(_comment_list);

var _timer = require('./../components/common/timer.js');

var _timer2 = _interopRequireDefault(_timer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var goodsDetail = function (_wepy$page) {
  _inherits(goodsDetail, _wepy$page);

  function goodsDetail() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, goodsDetail);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = goodsDetail.__proto__ || Object.getPrototypeOf(goodsDetail)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
      navigationBarTitleText: '商品详情'
    }, _this.data = {
      winWidth: 0,
      winHeight: '100%',
      goodsId: 0,
      detail: {},
      good_bigimg: [],
      //订单活动开始时间（格式yy-mm-dd 或者 yy/mm/dd ）
      //startTime: "2017-07-15 16:00:00",
      startTime: "",
      //订单活动结束时间（格式yy-mm-dd 或者 yy/mm/dd ）
      //endTime: "2017-07-21 16:04:00"
      endTime: "",
      hidden: true,
      //动画效果
      animationData: "",
      //购买方式:1-加购物车,2-立即购买
      orderType: 1,
      //购买数量
      orderNum: 1,
      //是否收藏
      isFavorite: false,
      isValidDate: true,
      canOrder: true, //是否能下单
      purchasetype: 1, //1-正常购买;2-补货
      purchaseText: "立即购买",
      special: 0, ////0-正常入库;1-特价专区和换货专区,
      commentList: [{
        url: "../images/icon_nav_01_new.png",
        name: "明天情人节我在写代码",
        time: "2017-10-01 10:10",
        content: "东西收到,很满意!!真的是超级好的卖家,解答疑问不厌其烦,细致认真,关键是东西好,而且货物发得超快,包装仔细,值得信赖!",
        start: 4.5,
        children: [{
          content: "跟你交易次次都这么成功和开心的．．希望我们以后有更多的交易吧．．．哈哈"
        }]
      }, {
        url: "../images/icon_nav_02_new.png",
        name: "勇闯天下",
        time: "2017-10-01 10:10",
        content: "太感谢了，衣服很漂亮，朋友很喜欢，最主要的是买家太好了~~~大大的赞一个。。。 衣服，很合身",
        start: 4,
        children: []
      }],
      commentList1: []
    }, _this.$repeat = {}, _this.$props = { "commentList": { "v-bind:list.sync": "commentList" }, "timer": { "xmlns:v-bind": "", "v-bind:startTime.sync": "startTime", "v-bind:endTime.sync": "endTime" } }, _this.$events = {}, _this.components = {
      commentList: _comment_list2.default,
      timer: _timer2.default
    }, _this.computed = {}, _this.events = {}, _this.methods = {
      // issus : https://mp.weixin.qq.com/debug/wxadoc/dev/api/ui-navigate.html#wxrelaunchobject
      homePage: function homePage() {
        _wepy2.default.switchTab({
          url: '/pages/home'
        });
        // wx.switchTab({
        //   url: '/pages/home'
        // })
        // console.log(wepy)
        // console.log(wx)
      },
      moreComment: function moreComment() {
        _wepy2.default.navigateTo({
          url: "/pages/comment"
        });
      },

      //let current = e.target.dataset.src;
      previewImage: function previewImage(e) {
        var current = e.target.dataset.src;

        var imageArry = [];
        var obj = this.detail.photoList;
        Object.keys(obj).forEach(function (item) {
          imageArry.push(obj[item].photo);
        });
        wx.previewImage({
          current: current, // 当前显示图片的http链接
          urls: imageArry // 需要预览的图片http链接列表
        });
      },
      bindOrderNumInput: function bindOrderNumInput(e) {
        this.orderNum = e.detail.value;
      },
      takeOrder: function takeOrder() {
        if (!this.canOrder) {
          return;
        }
        this.showConfirmData();
        this.orderType = 2;
        //this.doTakeOrder();
      },
      takeCart: function takeCart() {
        if (!this.canOrder) {
          return;
        }
        this.showConfirmData();
        this.orderType = 1;
        //this.doTakeCart();
      },
      takeFavorite: function takeFavorite() {
        if (this.isFavorite == true) {
          this.goodsUnFavorite();
        } else {
          this.goodsFavorite();
        }
      },
      closeModel: function closeModel() {
        var _this2 = this;

        this.winHeight = "100%";
        this.animation.height(0).step();
        this.setData({
          animationData: this.animation.export()
        });
        setTimeout(function () {
          _this2.hidden = true;
          _this2.$apply();
        }, 100);
      },
      confirmTake: function confirmTake() {
        //确定购物车或者直接购买
        if (this.orderType == 1) {
          this.doTakeCart();
        } else if (this.orderType == 2) {
          this.doTakeOrder();
        }
      },
      jiaBtnTap: function jiaBtnTap(e) {
        this.orderNum++;
      },
      jianBtnTap: function jianBtnTap() {
        if (this.orderNum > 1) {
          this.orderNum--;
        }
      },
      selAttr: function selAttr(e) {
        var id = e.currentTarget.dataset.id;
        var nameid = e.currentTarget.dataset.nameid;
        var index = e.currentTarget.dataset.index;
        for (var i = 0; i < this.detail.goodsSkuNameList.length; i++) {
          var skuValList = this.detail.goodsSkuNameList[i].skuValList;
          for (var j = 0; j < skuValList.length; j++) {
            var skuVal = skuValList[j];
            if (skuVal.skuValId == id) {
              if (skuVal.skuNameId == nameid) {
                skuVal.current = false;
                skuVal.current = true;
                this.detail.goodsSkuValIds[index] = id;
                for (var k = 0; k < this.detail.goodsSkuList.length; k++) {
                  var skuValIds = JSON.parse(this.detail.goodsSkuList[k].skuValIds).toArray;
                  console.log("goodskuids..." + this.detail.goodsSkuList[k].skuValIds);
                  console.log("this goodskuids..." + this.detail.goodsSkuValIds);
                  if ("[" + this.detail.goodsSkuValIds.toString() + "]" === this.detail.goodsSkuList[k].skuValIds) {
                    console.log("goodskuids equals...");
                    this.detail.stockNum = this.detail.goodsSkuList[k].stockNum;
                    this.detail.price = this.detail.goodsSkuList[k].price;
                    this.$apply();
                    break;
                  }
                }
              }
            }
          }
        }
      },

      onShareAppMessage: function onShareAppMessage(res) {
        if (res.from === 'button') {
          // 来自页面内转发按钮
          console.log(res.target);
        }
        return {
          title: this.detail.name,
          path: '/pages/goods_detail?id=' + this.goodsId,
          success: function success(res) {
            // 转发成功
          },
          fail: function fail(res) {
            // 转发失败
          }
        };
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(goodsDetail, [{
    key: 'onLoad',
    value: function onLoad(option) {
      var that = this;
      this.orderNum = 1;
      this.purchasetype = 1;
      this.isFavorite = false;
      this.isValidDate = true;
      this.canOrder = true;
      this.hidden = true;
      this.winHeight = "100%";
      that.detail = {};
      that.$apply();
      //接收上一个页面传过来的参数
      that.goodsId = option.id;
      if (option.purchasetype != undefined) {
        this.purchasetype = option.purchasetype;
      }
      if (this.purchasetype == 2) {
        this.purchaseText = "申请补货";
      } else {
        this.purchaseText = "立即购买";
      }
      if (option.special != undefined) {
        this.special = option.special;
      }
      that.getGoodsDetail();
      that.addUserBrowser();
      console.log("special===" + this.special);
    }
  }, {
    key: 'onShow',
    value: function onShow() {
      this.goodsIsFavorite();
      //创建动画
      var animation = wx.createAnimation({
        transformOrigin: "50% 50%",
        duration: 200,
        timingFunction: "linear",
        delay: 0
      });
      this.animation = animation;
    }
  }, {
    key: 'wxParseImgLoad',
    value: function wxParseImgLoad(e) {}
  }, {
    key: 'wxParseImgTap',
    value: function wxParseImgTap(e) {
      var that = this;
      var nowImgUrl = e.target.dataset.src;
      var tagFrom = e.target.dataset.from;
      if (typeof tagFrom != 'undefined' && tagFrom.length > 0) {
        wx.previewImage({
          current: nowImgUrl, // 当前显示图片的http链接
          // urls: that.data[tagFrom].imageUrls // 需要预览的图片http链接列表
          urls: that.bindData[tagFrom].imageUrls // 注释掉上面的 换着一行 (http://blog.csdn.net/zhuming3834/article/details/74380079)
        });
      }
    }
    /*onReachBottom() {
      let that = this;
      if (that.good_bigimg.length == 0) {
        that.good_bigimg = that.detail.good_bigimg;
        that.$apply();
      }
    }*/

  }, {
    key: 'getGoodsDetail',
    value: function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var that, json, time, data;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                that = this;
                //const json = await api.getGoodsDetail({

                _context.next = 3;
                return _api2.default.goodsDetail({
                  query: {
                    id: that.goodsId
                  }
                });

              case 3:
                json = _context.sent;
                time = {};

                if (json.data.code == 0) {
                  data = json.data.data;

                  that.detail = data;
                  _wxParse2.default.wxParse('detailInfo', 'html', that.detail.detailInfo, this);
                  time.endTime = that.detail.validEndTime;
                  time.startTime = that.detail.startTime;
                  if (json.data.validDate == "0") {
                    that.isValidDate = false;
                    if (this.purchasetype == 1 && this.special != 1) {
                      this.canOrder = false;
                    }
                  }
                } else {
                  if (json.data.msg) {
                    _tip2.default.error(json.data.msg);
                  } else {
                    _tip2.default.error('查看商品失败');
                  }
                }
                that.$apply();
                this.$invoke('timer', 'initTimer', time);

              case 8:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getGoodsDetail() {
        return _ref2.apply(this, arguments);
      }

      return getGoodsDetail;
    }()
  }, {
    key: 'addUserBrowser',
    value: function () {
      var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var that, userSpecialInfo, openId, json;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                that = this;
                userSpecialInfo = _wepy2.default.getStorageSync(_constant.USER_SPECICAL_INFO) || {};
                openId = userSpecialInfo.openid;
                _context2.next = 5;
                return _api2.default.addBrowser({
                  query: {
                    goodsId: that.goodsId,
                    openId: openId
                  }
                });

              case 5:
                json = _context2.sent;

              case 6:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function addUserBrowser() {
        return _ref3.apply(this, arguments);
      }

      return addUserBrowser;
    }()
  }, {
    key: 'doTakeCart',

    //加入购物车
    value: function () {
      var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var _this3 = this;

        var that, userSpecialInfo, openId, json;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                that = this;
                userSpecialInfo = _wepy2.default.getStorageSync(_constant.USER_SPECICAL_INFO) || {};
                openId = userSpecialInfo.openid;
                _context3.next = 5;
                return _api2.default.addCart({
                  query: {
                    openId: openId,
                    goodsId: that.goodsId,
                    goodsSkuId: this.detail.goodsSkuValIds,
                    purchaseType: this.purchasetype,
                    num: this.orderNum
                  }
                });

              case 5:
                json = _context3.sent;

                if (json.data.code == 0) {
                  this.winHeight = "100%";
                  this.animation.height(0).step();
                  this.setData({
                    animationData: this.animation.export()
                  });
                  setTimeout(function () {
                    _this3.hidden = true;
                    _this3.$apply();
                  }, 100);
                  _tip2.default.success("成功加入购物车");
                } else {
                  if (json.data.msg) {
                    _tip2.default.error(json.data.msg);
                  } else {
                    _tip2.default.error('无法加入购物车');
                  }
                }

              case 7:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function doTakeCart() {
        return _ref4.apply(this, arguments);
      }

      return doTakeCart;
    }()
    //立即购买

  }, {
    key: 'doTakeOrder',
    value: function () {
      var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        var _this4 = this;

        var that, userSpecialInfo, openId, json;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                that = this;
                userSpecialInfo = _wepy2.default.getStorageSync(_constant.USER_SPECICAL_INFO) || {};
                openId = userSpecialInfo.openid;
                _context4.next = 5;
                return _api2.default.addCart({
                  query: {
                    openId: openId,
                    goodsId: that.goodsId,
                    goodsSkuId: this.detail.goodsSkuValIds,
                    purchaseType: this.purchasetype,
                    num: this.orderNum
                  }
                });

              case 5:
                json = _context4.sent;

                if (json.data.code == 0) {
                  this.winHeight = "100%";
                  this.animation.height(0).step();
                  this.setData({
                    animationData: this.animation.export()
                  });
                  setTimeout(function () {
                    _this4.hidden = true;
                    _this4.$apply();
                  }, 100);
                  _wepy2.default.navigateTo({
                    url: "/pages/comfire_order?goodsId=" + that.goodsId + "&purchasetype=" + that.purchasetype
                  });
                } else {
                  if (json.data.msg) {
                    _tip2.default.error(json.data.msg);
                  } else {
                    _tip2.default.error('无法立刻购买');
                  }
                }

              case 7:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function doTakeOrder() {
        return _ref5.apply(this, arguments);
      }

      return doTakeOrder;
    }()
  }, {
    key: 'showConfirmData',
    value: function () {
      var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        var _this5 = this;

        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                this.animation.height('783rpx').step();
                this.setData({
                  animationData: this.animation.export()
                });
                setTimeout(function () {
                  _this5.hidden = false;
                  var systemInfo = _wepy2.default.getStorageSync(_constant.SYSTEM_INFO);
                  _this5.winHeight = systemInfo.windowHeight;
                  _this5.$apply();
                }, 100);

              case 3:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function showConfirmData() {
        return _ref6.apply(this, arguments);
      }

      return showConfirmData;
    }()
    //查看商品收藏状态

  }, {
    key: 'goodsIsFavorite',
    value: function () {
      var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
        var that, userSpecialInfo, openId, json;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                that = this;
                userSpecialInfo = _wepy2.default.getStorageSync(_constant.USER_SPECICAL_INFO) || {};
                openId = userSpecialInfo.openid;
                _context6.next = 5;
                return _api2.default.goodsIsFavorite({
                  query: {
                    openId: openId,
                    goodsId: that.goodsId
                  }
                });

              case 5:
                json = _context6.sent;

                if (json.data.code == 0) {
                  if (json.data.isFavorite == 1) {
                    this.isFavorite = true;
                    console.log(this.isFavorite);
                  } else {
                    this.isFavorite = false;
                  }
                } else {
                  console.log('查看商品收藏失败');
                  if (json.data.msg) {
                    _tip2.default.error(json.data.msg);
                  } else {
                    _tip2.default.error('查看商品收藏失败');
                  }
                }
                that.$apply();

              case 8:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function goodsIsFavorite() {
        return _ref7.apply(this, arguments);
      }

      return goodsIsFavorite;
    }()
    //商品收藏

  }, {
    key: 'goodsFavorite',
    value: function () {
      var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
        var that, userSpecialInfo, openId, json;
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                that = this;
                userSpecialInfo = _wepy2.default.getStorageSync(_constant.USER_SPECICAL_INFO) || {};
                openId = userSpecialInfo.openid;
                _context7.next = 5;
                return _api2.default.goodsFavorite({
                  query: {
                    openId: openId,
                    goodsId: that.goodsId
                  }
                });

              case 5:
                json = _context7.sent;

                if (json.data.code == 0) {
                  console.log("===========商品收藏成功=========");
                  this.isFavorite = true;
                  _tip2.default.toast("收藏成功");
                } else {
                  console.log(json.data);
                  _tip2.default.error('收藏失败');
                }
                that.$apply();

              case 8:
              case 'end':
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function goodsFavorite() {
        return _ref8.apply(this, arguments);
      }

      return goodsFavorite;
    }()
    //商品取消收藏

  }, {
    key: 'goodsUnFavorite',
    value: function () {
      var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
        var that, userSpecialInfo, openId, json;
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                that = this;
                userSpecialInfo = _wepy2.default.getStorageSync(_constant.USER_SPECICAL_INFO) || {};
                openId = userSpecialInfo.openid;
                _context8.next = 5;
                return _api2.default.goodsUnFavorite({
                  query: {
                    openId: openId,
                    goodsId: that.goodsId
                  }
                });

              case 5:
                json = _context8.sent;

                if (json.data.code == 0) {
                  console.log("===========商品取消收藏成功=========");
                  _tip2.default.toast("取消收藏成功");
                  this.isFavorite = false;
                } else {
                  console.log(json.data);
                  _tip2.default.error('取消收藏失败');
                }
                that.$apply();

              case 8:
              case 'end':
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function goodsUnFavorite() {
        return _ref9.apply(this, arguments);
      }

      return goodsUnFavorite;
    }()
  }]);

  return goodsDetail;
}(_wepy2.default.page);


Page(require('./../npm/wepy/lib/wepy.js').default.$createPage(goodsDetail , 'pages/goods_detail'));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdvb2RzX2RldGFpbC5qcyJdLCJuYW1lcyI6WyJnb29kc0RldGFpbCIsImNvbmZpZyIsIm5hdmlnYXRpb25CYXJUaXRsZVRleHQiLCJkYXRhIiwid2luV2lkdGgiLCJ3aW5IZWlnaHQiLCJnb29kc0lkIiwiZGV0YWlsIiwiZ29vZF9iaWdpbWciLCJzdGFydFRpbWUiLCJlbmRUaW1lIiwiaGlkZGVuIiwiYW5pbWF0aW9uRGF0YSIsIm9yZGVyVHlwZSIsIm9yZGVyTnVtIiwiaXNGYXZvcml0ZSIsImlzVmFsaWREYXRlIiwiY2FuT3JkZXIiLCJwdXJjaGFzZXR5cGUiLCJwdXJjaGFzZVRleHQiLCJzcGVjaWFsIiwiY29tbWVudExpc3QiLCJ1cmwiLCJuYW1lIiwidGltZSIsImNvbnRlbnQiLCJzdGFydCIsImNoaWxkcmVuIiwiY29tbWVudExpc3QxIiwiJHJlcGVhdCIsIiRwcm9wcyIsIiRldmVudHMiLCJjb21wb25lbnRzIiwiQ29tbWVudExpc3QiLCJ0aW1lciIsImNvbXB1dGVkIiwiZXZlbnRzIiwibWV0aG9kcyIsImhvbWVQYWdlIiwid2VweSIsInN3aXRjaFRhYiIsIm1vcmVDb21tZW50IiwibmF2aWdhdGVUbyIsInByZXZpZXdJbWFnZSIsImUiLCJjdXJyZW50IiwidGFyZ2V0IiwiZGF0YXNldCIsInNyYyIsImltYWdlQXJyeSIsIm9iaiIsInBob3RvTGlzdCIsIk9iamVjdCIsImtleXMiLCJmb3JFYWNoIiwiaXRlbSIsInB1c2giLCJwaG90byIsInd4IiwidXJscyIsImJpbmRPcmRlck51bUlucHV0IiwidmFsdWUiLCJ0YWtlT3JkZXIiLCJzaG93Q29uZmlybURhdGEiLCJ0YWtlQ2FydCIsInRha2VGYXZvcml0ZSIsImdvb2RzVW5GYXZvcml0ZSIsImdvb2RzRmF2b3JpdGUiLCJjbG9zZU1vZGVsIiwiYW5pbWF0aW9uIiwiaGVpZ2h0Iiwic3RlcCIsInNldERhdGEiLCJleHBvcnQiLCJzZXRUaW1lb3V0IiwiJGFwcGx5IiwiY29uZmlybVRha2UiLCJkb1Rha2VDYXJ0IiwiZG9UYWtlT3JkZXIiLCJqaWFCdG5UYXAiLCJqaWFuQnRuVGFwIiwic2VsQXR0ciIsImlkIiwiY3VycmVudFRhcmdldCIsIm5hbWVpZCIsImluZGV4IiwiaSIsImdvb2RzU2t1TmFtZUxpc3QiLCJsZW5ndGgiLCJza3VWYWxMaXN0IiwiaiIsInNrdVZhbCIsInNrdVZhbElkIiwic2t1TmFtZUlkIiwiZ29vZHNTa3VWYWxJZHMiLCJrIiwiZ29vZHNTa3VMaXN0Iiwic2t1VmFsSWRzIiwiSlNPTiIsInBhcnNlIiwidG9BcnJheSIsImNvbnNvbGUiLCJsb2ciLCJ0b1N0cmluZyIsInN0b2NrTnVtIiwicHJpY2UiLCJvblNoYXJlQXBwTWVzc2FnZSIsInJlcyIsImZyb20iLCJ0aXRsZSIsInBhdGgiLCJzdWNjZXNzIiwiZmFpbCIsIm9wdGlvbiIsInRoYXQiLCJ1bmRlZmluZWQiLCJnZXRHb29kc0RldGFpbCIsImFkZFVzZXJCcm93c2VyIiwiZ29vZHNJc0Zhdm9yaXRlIiwiY3JlYXRlQW5pbWF0aW9uIiwidHJhbnNmb3JtT3JpZ2luIiwiZHVyYXRpb24iLCJ0aW1pbmdGdW5jdGlvbiIsImRlbGF5Iiwibm93SW1nVXJsIiwidGFnRnJvbSIsImJpbmREYXRhIiwiaW1hZ2VVcmxzIiwiYXBpIiwicXVlcnkiLCJqc29uIiwiY29kZSIsIld4UGFyc2UiLCJ3eFBhcnNlIiwiZGV0YWlsSW5mbyIsInZhbGlkRW5kVGltZSIsInZhbGlkRGF0ZSIsIm1zZyIsInRpcCIsImVycm9yIiwiJGludm9rZSIsInVzZXJTcGVjaWFsSW5mbyIsImdldFN0b3JhZ2VTeW5jIiwiVVNFUl9TUEVDSUNBTF9JTkZPIiwib3BlbklkIiwib3BlbmlkIiwiYWRkQnJvd3NlciIsImFkZENhcnQiLCJnb29kc1NrdUlkIiwicHVyY2hhc2VUeXBlIiwibnVtIiwic3lzdGVtSW5mbyIsIlNZU1RFTV9JTkZPIiwid2luZG93SGVpZ2h0IiwidG9hc3QiLCJwYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFDQTs7OztBQUNBOztBQUlBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0lBQ3FCQSxXOzs7Ozs7Ozs7Ozs7OztnTUFDbkJDLE0sR0FBUztBQUNQQyw4QkFBd0I7QUFEakIsSyxRQUdUQyxJLEdBQU87QUFDTEMsZ0JBQVUsQ0FETDtBQUVMQyxpQkFBVyxNQUZOO0FBR0xDLGVBQVMsQ0FISjtBQUlMQyxjQUFRLEVBSkg7QUFLTEMsbUJBQWEsRUFMUjtBQU1MO0FBQ0E7QUFDQUMsaUJBQVcsRUFSTjtBQVNMO0FBQ0E7QUFDQUMsZUFBUyxFQVhKO0FBWUxDLGNBQVEsSUFaSDtBQWFMO0FBQ0FDLHFCQUFlLEVBZFY7QUFlTDtBQUNBQyxpQkFBVyxDQWhCTjtBQWlCTDtBQUNBQyxnQkFBVSxDQWxCTDtBQW1CTDtBQUNBQyxrQkFBWSxLQXBCUDtBQXFCTEMsbUJBQWEsSUFyQlI7QUFzQkxDLGdCQUFVLElBdEJMLEVBc0JXO0FBQ2hCQyxvQkFBYyxDQXZCVCxFQXVCWTtBQUNqQkMsb0JBQWMsTUF4QlQ7QUF5QkxDLGVBQVMsQ0F6QkosRUF5Qk87QUFDWkMsbUJBQWEsQ0FFWDtBQUNFQyxhQUFLLCtCQURQO0FBRUVDLGNBQU0sWUFGUjtBQUdFQyxjQUFNLGtCQUhSO0FBSUVDLGlCQUFTLDhEQUpYO0FBS0VDLGVBQU8sR0FMVDtBQU1FQyxrQkFBVSxDQUFDO0FBQ1RGLG1CQUFTO0FBREEsU0FBRDtBQU5aLE9BRlcsRUFZWDtBQUNFSCxhQUFLLCtCQURQO0FBRUVDLGNBQU0sTUFGUjtBQUdFQyxjQUFNLGtCQUhSO0FBSUVDLGlCQUFTLGdEQUpYO0FBS0VDLGVBQU8sQ0FMVDtBQU1FQyxrQkFBVTtBQU5aLE9BWlcsQ0ExQlI7QUFpRExDLG9CQUFjO0FBakRULEssUUFvRFJDLE8sR0FBVSxFLFFBQ1hDLE0sR0FBUyxFQUFDLGVBQWMsRUFBQyxvQkFBbUIsYUFBcEIsRUFBZixFQUFrRCxTQUFRLEVBQUMsZ0JBQWUsRUFBaEIsRUFBbUIseUJBQXdCLFdBQTNDLEVBQXVELHVCQUFzQixTQUE3RSxFQUExRCxFLFFBQ1RDLE8sR0FBVSxFLFFBQ1RDLFUsR0FBYTtBQUNWWCxtQkFBYVksc0JBREg7QUFFVkMsYUFBTUE7QUFGSSxLLFFBMEdaQyxRLEdBQVcsRSxRQUNYQyxNLEdBQVMsRSxRQXVKVEMsTyxHQUFVO0FBQ1I7QUFDQUMsY0FGUSxzQkFFRztBQUNUQyx1QkFBS0MsU0FBTCxDQUFlO0FBQ2JsQixlQUFLO0FBRFEsU0FBZjtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRCxPQVhPO0FBWVJtQixpQkFaUSx5QkFZTTtBQUNaRix1QkFBS0csVUFBTCxDQUFnQjtBQUNkcEIsZUFBSztBQURTLFNBQWhCO0FBR0QsT0FoQk87O0FBaUJOO0FBQ0ZxQixrQkFsQlEsd0JBa0JLQyxDQWxCTCxFQWtCUTtBQUNWLFlBQUlDLFVBQVVELEVBQUVFLE1BQUYsQ0FBU0MsT0FBVCxDQUFpQkMsR0FBL0I7O0FBRUosWUFBSUMsWUFBWSxFQUFoQjtBQUNBLFlBQUlDLE1BQU0sS0FBSzNDLE1BQUwsQ0FBWTRDLFNBQXRCO0FBQ0FDLGVBQU9DLElBQVAsQ0FBWUgsR0FBWixFQUFpQkksT0FBakIsQ0FBeUIsVUFBQ0MsSUFBRCxFQUFVO0FBQ2pDTixvQkFBVU8sSUFBVixDQUFlTixJQUFJSyxJQUFKLEVBQVVFLEtBQXpCO0FBQ0QsU0FGRDtBQUdBQyxXQUFHZixZQUFILENBQWdCO0FBQ2RFLG1CQUFTQSxPQURLLEVBQ0k7QUFDbEJjLGdCQUFNVixTQUZRLENBRUc7QUFGSCxTQUFoQjtBQUlELE9BOUJPO0FBK0JSVyx1QkEvQlEsNkJBK0JVaEIsQ0EvQlYsRUErQmE7QUFDbkIsYUFBSzlCLFFBQUwsR0FBZ0I4QixFQUFFckMsTUFBRixDQUFTc0QsS0FBekI7QUFDRCxPQWpDTztBQWtDUkMsZUFsQ1EsdUJBa0NJO0FBQ1YsWUFBSSxDQUFDLEtBQUs3QyxRQUFWLEVBQW9CO0FBQ2xCO0FBQ0Q7QUFDRCxhQUFLOEMsZUFBTDtBQUNBLGFBQUtsRCxTQUFMLEdBQWlCLENBQWpCO0FBQ0E7QUFDRCxPQXpDTztBQTBDUm1ELGNBMUNRLHNCQTBDRztBQUNULFlBQUksQ0FBQyxLQUFLL0MsUUFBVixFQUFvQjtBQUNsQjtBQUNEO0FBQ0QsYUFBSzhDLGVBQUw7QUFDQSxhQUFLbEQsU0FBTCxHQUFpQixDQUFqQjtBQUNBO0FBQ0QsT0FqRE87QUFrRFJvRCxrQkFsRFEsMEJBa0RPO0FBQ2IsWUFBSSxLQUFLbEQsVUFBTCxJQUFtQixJQUF2QixFQUE2QjtBQUMzQixlQUFLbUQsZUFBTDtBQUNELFNBRkQsTUFFTztBQUNMLGVBQUtDLGFBQUw7QUFDRDtBQUNGLE9BeERPO0FBeURSQyxnQkF6RFEsd0JBeURLO0FBQUE7O0FBQ1gsYUFBSy9ELFNBQUwsR0FBaUIsTUFBakI7QUFDQSxhQUFLZ0UsU0FBTCxDQUFlQyxNQUFmLENBQXNCLENBQXRCLEVBQXlCQyxJQUF6QjtBQUNBLGFBQUtDLE9BQUwsQ0FBYTtBQUNYNUQseUJBQWUsS0FBS3lELFNBQUwsQ0FBZUksTUFBZjtBQURKLFNBQWI7QUFHQUMsbUJBQVcsWUFBTTtBQUNmLGlCQUFLL0QsTUFBTCxHQUFjLElBQWQ7QUFDQSxpQkFBS2dFLE1BQUw7QUFDRCxTQUhELEVBR0csR0FISDtBQUlELE9BbkVPO0FBb0VSQyxpQkFwRVEseUJBb0VNO0FBQUU7QUFDZCxZQUFJLEtBQUsvRCxTQUFMLElBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCLGVBQUtnRSxVQUFMO0FBQ0QsU0FGRCxNQUVPLElBQUksS0FBS2hFLFNBQUwsSUFBa0IsQ0FBdEIsRUFBeUI7QUFDOUIsZUFBS2lFLFdBQUw7QUFDRDtBQUNGLE9BMUVPO0FBMkVSQyxlQTNFUSxxQkEyRUVuQyxDQTNFRixFQTJFSztBQUNYLGFBQUs5QixRQUFMO0FBQ0QsT0E3RU87QUE4RVJrRSxnQkE5RVEsd0JBOEVLO0FBQ1gsWUFBSSxLQUFLbEUsUUFBTCxHQUFnQixDQUFwQixFQUF1QjtBQUNyQixlQUFLQSxRQUFMO0FBQ0Q7QUFDRixPQWxGTztBQW1GUm1FLGFBbkZRLG1CQW1GQXJDLENBbkZBLEVBbUZHO0FBQ1QsWUFBSXNDLEtBQUt0QyxFQUFFdUMsYUFBRixDQUFnQnBDLE9BQWhCLENBQXdCbUMsRUFBakM7QUFDQSxZQUFJRSxTQUFTeEMsRUFBRXVDLGFBQUYsQ0FBZ0JwQyxPQUFoQixDQUF3QnFDLE1BQXJDO0FBQ0EsWUFBSUMsUUFBUXpDLEVBQUV1QyxhQUFGLENBQWdCcEMsT0FBaEIsQ0FBd0JzQyxLQUFwQztBQUNBLGFBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUsvRSxNQUFMLENBQVlnRixnQkFBWixDQUE2QkMsTUFBakQsRUFBeURGLEdBQXpELEVBQThEO0FBQzVELGNBQUlHLGFBQWEsS0FBS2xGLE1BQUwsQ0FBWWdGLGdCQUFaLENBQTZCRCxDQUE3QixFQUFnQ0csVUFBakQ7QUFDQSxlQUFLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSUQsV0FBV0QsTUFBL0IsRUFBdUNFLEdBQXZDLEVBQTRDO0FBQzFDLGdCQUFJQyxTQUFTRixXQUFXQyxDQUFYLENBQWI7QUFDRSxnQkFBSUMsT0FBT0MsUUFBUCxJQUFtQlYsRUFBdkIsRUFBMkI7QUFDN0Isa0JBQUlTLE9BQU9FLFNBQVAsSUFBb0JULE1BQXhCLEVBQWdDO0FBQzlCTyx1QkFBTzlDLE9BQVAsR0FBaUIsS0FBakI7QUFDRThDLHVCQUFPOUMsT0FBUCxHQUFpQixJQUFqQjtBQUNBLHFCQUFLdEMsTUFBTCxDQUFZdUYsY0FBWixDQUEyQlQsS0FBM0IsSUFBb0NILEVBQXBDO0FBQ0EscUJBQUssSUFBSWEsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUt4RixNQUFMLENBQVl5RixZQUFaLENBQXlCUixNQUE3QyxFQUFxRE8sR0FBckQsRUFBMEQ7QUFDeEQsc0JBQUlFLFlBQVlDLEtBQUtDLEtBQUwsQ0FBVyxLQUFLNUYsTUFBTCxDQUFZeUYsWUFBWixDQUF5QkQsQ0FBekIsRUFBNEJFLFNBQXZDLEVBQWtERyxPQUFsRTtBQUNBQywwQkFBUUMsR0FBUixDQUFZLGtCQUFrQixLQUFLL0YsTUFBTCxDQUFZeUYsWUFBWixDQUF5QkQsQ0FBekIsRUFBNEJFLFNBQTFEO0FBQ0FJLDBCQUFRQyxHQUFSLENBQVksdUJBQXVCLEtBQUsvRixNQUFMLENBQVl1RixjQUEvQztBQUNBLHNCQUFJLE1BQU0sS0FBS3ZGLE1BQUwsQ0FBWXVGLGNBQVosQ0FBMkJTLFFBQTNCLEVBQU4sR0FBOEMsR0FBOUMsS0FBc0QsS0FBS2hHLE1BQUwsQ0FBWXlGLFlBQVosQ0FBeUJELENBQXpCLEVBQTRCRSxTQUF0RixFQUFpRztBQUMvRkksNEJBQVFDLEdBQVIsQ0FBWSxzQkFBWjtBQUNBLHlCQUFLL0YsTUFBTCxDQUFZaUcsUUFBWixHQUF1QixLQUFLakcsTUFBTCxDQUFZeUYsWUFBWixDQUF5QkQsQ0FBekIsRUFBNEJTLFFBQW5EO0FBQ0EseUJBQUtqRyxNQUFMLENBQVlrRyxLQUFaLEdBQW9CLEtBQUtsRyxNQUFMLENBQVl5RixZQUFaLENBQXlCRCxDQUF6QixFQUE0QlUsS0FBaEQ7QUFDQSx5QkFBSzlCLE1BQUw7QUFDQTtBQUNEO0FBQ0Y7QUFDRjtBQUNGO0FBQ0Y7QUFDRjtBQUNGLE9BaEhPOztBQWlIUitCLHlCQUFtQiwyQkFBU0MsR0FBVCxFQUFjO0FBQy9CLFlBQUlBLElBQUlDLElBQUosS0FBYSxRQUFqQixFQUEyQjtBQUN6QjtBQUNBUCxrQkFBUUMsR0FBUixDQUFZSyxJQUFJN0QsTUFBaEI7QUFDRDtBQUNELGVBQU87QUFDTCtELGlCQUFPLEtBQUt0RyxNQUFMLENBQVlnQixJQURkO0FBRUx1RixnQkFBTSw0QkFBNEIsS0FBS3hHLE9BRmxDO0FBR0x5RyxtQkFBUyxpQkFBU0osR0FBVCxFQUFjO0FBQ3JCO0FBQ0QsV0FMSTtBQU1MSyxnQkFBTSxjQUFTTCxHQUFULEVBQWM7QUFDbEI7QUFDRDtBQVJJLFNBQVA7QUFVRDtBQWhJTyxLOzs7OzsyQkE5UEhNLE0sRUFBUTtBQUNiLFVBQUlDLE9BQU8sSUFBWDtBQUNBLFdBQUtwRyxRQUFMLEdBQWdCLENBQWhCO0FBQ0EsV0FBS0ksWUFBTCxHQUFvQixDQUFwQjtBQUNBLFdBQUtILFVBQUwsR0FBa0IsS0FBbEI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUtOLE1BQUwsR0FBYyxJQUFkO0FBQ0EsV0FBS04sU0FBTCxHQUFpQixNQUFqQjtBQUNBNkcsV0FBSzNHLE1BQUwsR0FBYyxFQUFkO0FBQ0EyRyxXQUFLdkMsTUFBTDtBQUNBO0FBQ0F1QyxXQUFLNUcsT0FBTCxHQUFlMkcsT0FBTy9CLEVBQXRCO0FBQ0EsVUFBSStCLE9BQU8vRixZQUFQLElBQXVCaUcsU0FBM0IsRUFBc0M7QUFDcEMsYUFBS2pHLFlBQUwsR0FBb0IrRixPQUFPL0YsWUFBM0I7QUFDRDtBQUNELFVBQUksS0FBS0EsWUFBTCxJQUFxQixDQUF6QixFQUE0QjtBQUMxQixhQUFLQyxZQUFMLEdBQW9CLE1BQXBCO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsYUFBS0EsWUFBTCxHQUFvQixNQUFwQjtBQUNEO0FBQ0QsVUFBSThGLE9BQU83RixPQUFQLElBQWtCK0YsU0FBdEIsRUFBaUM7QUFDL0IsYUFBSy9GLE9BQUwsR0FBZTZGLE9BQU83RixPQUF0QjtBQUNEO0FBQ0Q4RixXQUFLRSxjQUFMO0FBQ0FGLFdBQUtHLGNBQUw7QUFDQWhCLGNBQVFDLEdBQVIsQ0FBWSxlQUFlLEtBQUtsRixPQUFoQztBQUNEOzs7NkJBQ1E7QUFDUCxXQUFLa0csZUFBTDtBQUNBO0FBQ0EsVUFBSWpELFlBQVlYLEdBQUc2RCxlQUFILENBQW1CO0FBQ2pDQyx5QkFBaUIsU0FEZ0I7QUFFakNDLGtCQUFVLEdBRnVCO0FBR2pDQyx3QkFBZ0IsUUFIaUI7QUFJakNDLGVBQU87QUFKMEIsT0FBbkIsQ0FBaEI7QUFNQSxXQUFLdEQsU0FBTCxHQUFpQkEsU0FBakI7QUFDRDs7O21DQUNjekIsQyxFQUFHLENBQUU7OztrQ0FFTkEsQyxFQUFHO0FBQ2YsVUFBSXNFLE9BQU8sSUFBWDtBQUNBLFVBQUlVLFlBQVloRixFQUFFRSxNQUFGLENBQVNDLE9BQVQsQ0FBaUJDLEdBQWpDO0FBQ0EsVUFBSTZFLFVBQVVqRixFQUFFRSxNQUFGLENBQVNDLE9BQVQsQ0FBaUI2RCxJQUEvQjtBQUNBLFVBQUksT0FBT2lCLE9BQVAsSUFBbUIsV0FBbkIsSUFBa0NBLFFBQVFyQyxNQUFSLEdBQWlCLENBQXZELEVBQTBEO0FBQ3hEOUIsV0FBR2YsWUFBSCxDQUFnQjtBQUNkRSxtQkFBUytFLFNBREssRUFDTTtBQUNwQjtBQUNBakUsZ0JBQU11RCxLQUFLWSxRQUFMLENBQWNELE9BQWQsRUFBdUJFLFNBSGYsQ0FHeUI7QUFIekIsU0FBaEI7QUFLRDtBQUNGO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBUU1iLG9CLEdBQU8sSTtBQUNYOzs7dUJBQ21CYyxjQUFJaEksV0FBSixDQUFnQjtBQUNqQ2lJLHlCQUFPO0FBQ0wvQyx3QkFBSWdDLEtBQUs1RztBQURKO0FBRDBCLGlCQUFoQixDOzs7QUFBYjRILG9CO0FBS0YxRyxvQixHQUFPLEU7O0FBQ1gsb0JBQUkwRyxLQUFLL0gsSUFBTCxDQUFVZ0ksSUFBVixJQUFrQixDQUF0QixFQUF5QjtBQUNuQmhJLHNCQURtQixHQUNaK0gsS0FBSy9ILElBQUwsQ0FBVUEsSUFERTs7QUFFdkIrRyx1QkFBSzNHLE1BQUwsR0FBY0osSUFBZDtBQUNBaUksb0NBQVFDLE9BQVIsQ0FBZ0IsWUFBaEIsRUFBOEIsTUFBOUIsRUFBc0NuQixLQUFLM0csTUFBTCxDQUFZK0gsVUFBbEQsRUFBOEQsSUFBOUQ7QUFDQTlHLHVCQUFLZCxPQUFMLEdBQWV3RyxLQUFLM0csTUFBTCxDQUFZZ0ksWUFBM0I7QUFDQS9HLHVCQUFLZixTQUFMLEdBQWlCeUcsS0FBSzNHLE1BQUwsQ0FBWUUsU0FBN0I7QUFDQSxzQkFBSXlILEtBQUsvSCxJQUFMLENBQVVxSSxTQUFWLElBQXVCLEdBQTNCLEVBQWdDO0FBQzlCdEIseUJBQUtsRyxXQUFMLEdBQW1CLEtBQW5CO0FBQ0Esd0JBQUksS0FBS0UsWUFBTCxJQUFxQixDQUFyQixJQUEwQixLQUFLRSxPQUFMLElBQWdCLENBQTlDLEVBQWlEO0FBQy9DLDJCQUFLSCxRQUFMLEdBQWdCLEtBQWhCO0FBQ0Q7QUFDRjtBQUNGLGlCQVpELE1BWU87QUFDTCxzQkFBSWlILEtBQUsvSCxJQUFMLENBQVVzSSxHQUFkLEVBQW1CO0FBQ2pCQyxrQ0FBSUMsS0FBSixDQUFVVCxLQUFLL0gsSUFBTCxDQUFVc0ksR0FBcEI7QUFDRCxtQkFGRCxNQUVPO0FBQ0xDLGtDQUFJQyxLQUFKLENBQVUsUUFBVjtBQUNEO0FBQ0Y7QUFDRHpCLHFCQUFLdkMsTUFBTDtBQUNBLHFCQUFLaUUsT0FBTCxDQUFhLE9BQWIsRUFBc0IsV0FBdEIsRUFBbUNwSCxJQUFuQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdJMEYsb0IsR0FBTyxJO0FBQ1AyQiwrQixHQUFrQnRHLGVBQUt1RyxjQUFMLENBQW9CQyw0QkFBcEIsS0FBMkMsRTtBQUM3REMsc0IsR0FBU0gsZ0JBQWdCSSxNOzt1QkFDVmpCLGNBQUlrQixVQUFKLENBQWU7QUFDaENqQix5QkFBTztBQUNMM0gsNkJBQVM0RyxLQUFLNUcsT0FEVDtBQUVMMEksNEJBQVFBO0FBRkg7QUFEeUIsaUJBQWYsQzs7O0FBQWJkLG9COzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBU1I7Ozs7Ozs7Ozs7QUFFTWhCLG9CLEdBQU8sSTtBQUNQMkIsK0IsR0FBa0J0RyxlQUFLdUcsY0FBTCxDQUFvQkMsNEJBQXBCLEtBQTJDLEU7QUFDN0RDLHNCLEdBQVNILGdCQUFnQkksTTs7dUJBQ1ZqQixjQUFJbUIsT0FBSixDQUFZO0FBQzdCbEIseUJBQU87QUFDTGUsNEJBQVFBLE1BREg7QUFFTDFJLDZCQUFTNEcsS0FBSzVHLE9BRlQ7QUFHTDhJLGdDQUFZLEtBQUs3SSxNQUFMLENBQVl1RixjQUhuQjtBQUlMdUQsa0NBQWMsS0FBS25JLFlBSmQ7QUFLTG9JLHlCQUFLLEtBQUt4STtBQUxMO0FBRHNCLGlCQUFaLEM7OztBQUFib0gsb0I7O0FBU04sb0JBQUlBLEtBQUsvSCxJQUFMLENBQVVnSSxJQUFWLElBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCLHVCQUFLOUgsU0FBTCxHQUFpQixNQUFqQjtBQUNBLHVCQUFLZ0UsU0FBTCxDQUFlQyxNQUFmLENBQXNCLENBQXRCLEVBQXlCQyxJQUF6QjtBQUNBLHVCQUFLQyxPQUFMLENBQWE7QUFDWDVELG1DQUFlLEtBQUt5RCxTQUFMLENBQWVJLE1BQWY7QUFESixtQkFBYjtBQUdBQyw2QkFBVyxZQUFNO0FBQ2YsMkJBQUsvRCxNQUFMLEdBQWMsSUFBZDtBQUNBLDJCQUFLZ0UsTUFBTDtBQUNELG1CQUhELEVBR0csR0FISDtBQUlBK0QsZ0NBQUkzQixPQUFKLENBQVksU0FBWjtBQUNELGlCQVhELE1BV087QUFDTCxzQkFBSW1CLEtBQUsvSCxJQUFMLENBQVVzSSxHQUFkLEVBQW1CO0FBQ2pCQyxrQ0FBSUMsS0FBSixDQUFVVCxLQUFLL0gsSUFBTCxDQUFVc0ksR0FBcEI7QUFDRCxtQkFGRCxNQUVPO0FBQ0xDLGtDQUFJQyxLQUFKLENBQVUsU0FBVjtBQUNEO0FBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFSDs7Ozs7Ozs7Ozs7OztBQUVNekIsb0IsR0FBTyxJO0FBQ1AyQiwrQixHQUFrQnRHLGVBQUt1RyxjQUFMLENBQW9CQyw0QkFBcEIsS0FBMkMsRTtBQUM3REMsc0IsR0FBU0gsZ0JBQWdCSSxNOzt1QkFDVmpCLGNBQUltQixPQUFKLENBQVk7QUFDN0JsQix5QkFBTztBQUNMZSw0QkFBUUEsTUFESDtBQUVMMUksNkJBQVM0RyxLQUFLNUcsT0FGVDtBQUdMOEksZ0NBQVksS0FBSzdJLE1BQUwsQ0FBWXVGLGNBSG5CO0FBSUx1RCxrQ0FBYyxLQUFLbkksWUFKZDtBQUtMb0kseUJBQUssS0FBS3hJO0FBTEw7QUFEc0IsaUJBQVosQzs7O0FBQWJvSCxvQjs7QUFTTixvQkFBSUEsS0FBSy9ILElBQUwsQ0FBVWdJLElBQVYsSUFBa0IsQ0FBdEIsRUFBeUI7QUFDdkIsdUJBQUs5SCxTQUFMLEdBQWlCLE1BQWpCO0FBQ0EsdUJBQUtnRSxTQUFMLENBQWVDLE1BQWYsQ0FBc0IsQ0FBdEIsRUFBeUJDLElBQXpCO0FBQ0EsdUJBQUtDLE9BQUwsQ0FBYTtBQUNYNUQsbUNBQWUsS0FBS3lELFNBQUwsQ0FBZUksTUFBZjtBQURKLG1CQUFiO0FBR0FDLDZCQUFXLFlBQU07QUFDZiwyQkFBSy9ELE1BQUwsR0FBYyxJQUFkO0FBQ0EsMkJBQUtnRSxNQUFMO0FBQ0QsbUJBSEQsRUFHRyxHQUhIO0FBSUFwQyxpQ0FBS0csVUFBTCxDQUFnQjtBQUNkcEIseUJBQUssa0NBQWtDNEYsS0FBSzVHLE9BQXZDLEdBQWlELGdCQUFqRCxHQUFvRTRHLEtBQUtoRztBQURoRSxtQkFBaEI7QUFHRCxpQkFiRCxNQWFPO0FBQ0wsc0JBQUlnSCxLQUFLL0gsSUFBTCxDQUFVc0ksR0FBZCxFQUFtQjtBQUNqQkMsa0NBQUlDLEtBQUosQ0FBVVQsS0FBSy9ILElBQUwsQ0FBVXNJLEdBQXBCO0FBQ0QsbUJBRkQsTUFFTztBQUNMQyxrQ0FBSUMsS0FBSixDQUFVLFFBQVY7QUFDRDtBQUNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdELHFCQUFLdEUsU0FBTCxDQUFlQyxNQUFmLENBQXNCLFFBQXRCLEVBQWdDQyxJQUFoQztBQUNBLHFCQUFLQyxPQUFMLENBQWE7QUFDWDVELGlDQUFlLEtBQUt5RCxTQUFMLENBQWVJLE1BQWY7QUFESixpQkFBYjtBQUdBQywyQkFBVyxZQUFNO0FBQ2YseUJBQUsvRCxNQUFMLEdBQWMsS0FBZDtBQUNBLHNCQUFJNEksYUFBYWhILGVBQUt1RyxjQUFMLENBQW9CVSxxQkFBcEIsQ0FBakI7QUFDQSx5QkFBS25KLFNBQUwsR0FBaUJrSixXQUFXRSxZQUE1QjtBQUNBLHlCQUFLOUUsTUFBTDtBQUNELGlCQUxELEVBS0csR0FMSDs7Ozs7Ozs7Ozs7Ozs7OztBQU9GOzs7Ozs7Ozs7OztBQUVNdUMsb0IsR0FBTyxJO0FBQ1AyQiwrQixHQUFrQnRHLGVBQUt1RyxjQUFMLENBQW9CQyw0QkFBcEIsS0FBMkMsRTtBQUM3REMsc0IsR0FBU0gsZ0JBQWdCSSxNOzt1QkFDVmpCLGNBQUlWLGVBQUosQ0FBb0I7QUFDckNXLHlCQUFPO0FBQ0xlLDRCQUFRQSxNQURIO0FBRUwxSSw2QkFBUzRHLEtBQUs1RztBQUZUO0FBRDhCLGlCQUFwQixDOzs7QUFBYjRILG9COztBQU1OLG9CQUFJQSxLQUFLL0gsSUFBTCxDQUFVZ0ksSUFBVixJQUFrQixDQUF0QixFQUF5QjtBQUN2QixzQkFBSUQsS0FBSy9ILElBQUwsQ0FBVVksVUFBVixJQUF3QixDQUE1QixFQUErQjtBQUM3Qix5QkFBS0EsVUFBTCxHQUFrQixJQUFsQjtBQUNBc0YsNEJBQVFDLEdBQVIsQ0FBWSxLQUFLdkYsVUFBakI7QUFDRCxtQkFIRCxNQUdPO0FBQ0wseUJBQUtBLFVBQUwsR0FBa0IsS0FBbEI7QUFDRDtBQUNGLGlCQVBELE1BT087QUFDTHNGLDBCQUFRQyxHQUFSLENBQVksVUFBWjtBQUNBLHNCQUFJNEIsS0FBSy9ILElBQUwsQ0FBVXNJLEdBQWQsRUFBbUI7QUFDakJDLGtDQUFJQyxLQUFKLENBQVVULEtBQUsvSCxJQUFMLENBQVVzSSxHQUFwQjtBQUNELG1CQUZELE1BRU87QUFDTEMsa0NBQUlDLEtBQUosQ0FBVSxVQUFWO0FBQ0Q7QUFDRjtBQUNEekIscUJBQUt2QyxNQUFMOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUY7Ozs7Ozs7Ozs7O0FBRU11QyxvQixHQUFPLEk7QUFDUDJCLCtCLEdBQWtCdEcsZUFBS3VHLGNBQUwsQ0FBb0JDLDRCQUFwQixLQUEyQyxFO0FBQzdEQyxzQixHQUFTSCxnQkFBZ0JJLE07O3VCQUNWakIsY0FBSTdELGFBQUosQ0FBa0I7QUFDbkM4RCx5QkFBTztBQUNMZSw0QkFBUUEsTUFESDtBQUVMMUksNkJBQVM0RyxLQUFLNUc7QUFGVDtBQUQ0QixpQkFBbEIsQzs7O0FBQWI0SCxvQjs7QUFNTixvQkFBSUEsS0FBSy9ILElBQUwsQ0FBVWdJLElBQVYsSUFBa0IsQ0FBdEIsRUFBeUI7QUFDdkI5QiwwQkFBUUMsR0FBUixDQUFZLDRCQUFaO0FBQ0EsdUJBQUt2RixVQUFMLEdBQWtCLElBQWxCO0FBQ0EySCxnQ0FBSWdCLEtBQUosQ0FBVSxNQUFWO0FBQ0QsaUJBSkQsTUFJTztBQUNMckQsMEJBQVFDLEdBQVIsQ0FBWTRCLEtBQUsvSCxJQUFqQjtBQUNBdUksZ0NBQUlDLEtBQUosQ0FBVSxNQUFWO0FBQ0Q7QUFDRHpCLHFCQUFLdkMsTUFBTDs7Ozs7Ozs7Ozs7Ozs7OztBQUVGOzs7Ozs7Ozs7OztBQUVNdUMsb0IsR0FBTyxJO0FBQ1AyQiwrQixHQUFrQnRHLGVBQUt1RyxjQUFMLENBQW9CQyw0QkFBcEIsS0FBMkMsRTtBQUM3REMsc0IsR0FBU0gsZ0JBQWdCSSxNOzt1QkFDVmpCLGNBQUk5RCxlQUFKLENBQW9CO0FBQ3JDK0QseUJBQU87QUFDTGUsNEJBQVFBLE1BREg7QUFFTDFJLDZCQUFTNEcsS0FBSzVHO0FBRlQ7QUFEOEIsaUJBQXBCLEM7OztBQUFiNEgsb0I7O0FBTU4sb0JBQUlBLEtBQUsvSCxJQUFMLENBQVVnSSxJQUFWLElBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCOUIsMEJBQVFDLEdBQVIsQ0FBWSw4QkFBWjtBQUNBb0MsZ0NBQUlnQixLQUFKLENBQVUsUUFBVjtBQUNBLHVCQUFLM0ksVUFBTCxHQUFrQixLQUFsQjtBQUNELGlCQUpELE1BSU87QUFDTHNGLDBCQUFRQyxHQUFSLENBQVk0QixLQUFLL0gsSUFBakI7QUFDQXVJLGdDQUFJQyxLQUFKLENBQVUsUUFBVjtBQUNEO0FBQ0R6QixxQkFBS3ZDLE1BQUw7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUEzVHFDcEMsZUFBS29ILEk7O2tCQUF6QjNKLFciLCJmaWxlIjoiZ29vZHNfZGV0YWlsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgd2VweSBmcm9tICd3ZXB5J1xuaW1wb3J0IHtcbiAgU1lTVEVNX0lORk8sXG4gIFVTRVJfU1BFQ0lDQUxfSU5GT1xufSBmcm9tICdAL3V0aWxzL2NvbnN0YW50JztcbmltcG9ydCB0aXAgZnJvbSAnQC91dGlscy90aXAnXG5pbXBvcnQgYXBpIGZyb20gJ0AvYXBpL2FwaSc7XG5pbXBvcnQgV3hQYXJzZSBmcm9tIFwiLi4vcGx1Z2lucy93eFBhcnNlL3d4UGFyc2VcIjtcbmltcG9ydCBDb21tZW50TGlzdCBmcm9tIFwiLi4vY29tcG9uZW50cy9jb21tZW50X2xpc3RcIlxuaW1wb3J0IHRpbWVyIGZyb20gJ0AvY29tcG9uZW50cy9jb21tb24vdGltZXInXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBnb29kc0RldGFpbCBleHRlbmRzIHdlcHkucGFnZSB7XG4gIGNvbmZpZyA9IHtcbiAgICBuYXZpZ2F0aW9uQmFyVGl0bGVUZXh0OiAn5ZWG5ZOB6K+m5oOFJ1xuICB9XG4gIGRhdGEgPSB7XG4gICAgd2luV2lkdGg6IDAsXG4gICAgd2luSGVpZ2h0OiAnMTAwJScsXG4gICAgZ29vZHNJZDogMCxcbiAgICBkZXRhaWw6IHt9LFxuICAgIGdvb2RfYmlnaW1nOiBbXSxcbiAgICAvL+iuouWNlea0u+WKqOW8gOWni+aXtumXtO+8iOagvOW8j3l5LW1tLWRkIOaIluiAhSB5eS9tbS9kZCDvvIlcbiAgICAvL3N0YXJ0VGltZTogXCIyMDE3LTA3LTE1IDE2OjAwOjAwXCIsXG4gICAgc3RhcnRUaW1lOiBcIlwiLFxuICAgIC8v6K6i5Y2V5rS75Yqo57uT5p2f5pe26Ze077yI5qC85byPeXktbW0tZGQg5oiW6ICFIHl5L21tL2RkIO+8iVxuICAgIC8vZW5kVGltZTogXCIyMDE3LTA3LTIxIDE2OjA0OjAwXCJcbiAgICBlbmRUaW1lOiBcIlwiLFxuICAgIGhpZGRlbjogdHJ1ZSxcbiAgICAvL+WKqOeUu+aViOaenFxuICAgIGFuaW1hdGlvbkRhdGE6IFwiXCIsXG4gICAgLy/otK3kubDmlrnlvI86MS3liqDotK3nianovaYsMi3nq4vljbPotK3kubBcbiAgICBvcmRlclR5cGU6IDEsXG4gICAgLy/otK3kubDmlbDph49cbiAgICBvcmRlck51bTogMSxcbiAgICAvL+aYr+WQpuaUtuiXj1xuICAgIGlzRmF2b3JpdGU6IGZhbHNlLFxuICAgIGlzVmFsaWREYXRlOiB0cnVlLFxuICAgIGNhbk9yZGVyOiB0cnVlLCAvL+aYr+WQpuiDveS4i+WNlVxuICAgIHB1cmNoYXNldHlwZTogMSwgLy8xLeato+W4uOi0reS5sDsyLeihpei0p1xuICAgIHB1cmNoYXNlVGV4dDogXCLnq4vljbPotK3kubBcIixcbiAgICBzcGVjaWFsOiAwLCAvLy8vMC3mraPluLjlhaXlupM7MS3nibnku7fkuJPljLrlkozmjaLotKfkuJPljLosXG4gICAgY29tbWVudExpc3Q6IFtcblxuICAgICAge1xuICAgICAgICB1cmw6IFwiLi4vaW1hZ2VzL2ljb25fbmF2XzAxX25ldy5wbmdcIixcbiAgICAgICAgbmFtZTogXCLmmI7lpKnmg4XkurroioLmiJHlnKjlhpnku6PnoIFcIixcbiAgICAgICAgdGltZTogXCIyMDE3LTEwLTAxIDEwOjEwXCIsXG4gICAgICAgIGNvbnRlbnQ6IFwi5Lic6KW/5pS25YiwLOW+iOa7oeaEjyEh55yf55qE5piv6LaF57qn5aW955qE5Y2W5a62LOino+etlOeWkemXruS4jeWOjOWFtueDpiznu4boh7TorqTnnJ8s5YWz6ZSu5piv5Lic6KW/5aW9LOiAjOS4lOi0p+eJqeWPkeW+l+i2heW/qyzljIXoo4Xku5Tnu4Ys5YC85b6X5L+h6LWWIVwiLFxuICAgICAgICBzdGFydDogNC41LFxuICAgICAgICBjaGlsZHJlbjogW3tcbiAgICAgICAgICBjb250ZW50OiBcIui3n+S9oOS6pOaYk+asoeasoemDvei/meS5iOaIkOWKn+WSjOW8gOW/g+eahO+8ju+8juW4jOacm+aIkeS7rOS7peWQjuacieabtOWkmueahOS6pOaYk+WQp++8ju+8ju+8juWTiOWTiFwiXG4gICAgICAgIH1dXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB1cmw6IFwiLi4vaW1hZ2VzL2ljb25fbmF2XzAyX25ldy5wbmdcIixcbiAgICAgICAgbmFtZTogXCLli4fpl6/lpKnkuItcIixcbiAgICAgICAgdGltZTogXCIyMDE3LTEwLTAxIDEwOjEwXCIsXG4gICAgICAgIGNvbnRlbnQ6IFwi5aSq5oSf6LCi5LqG77yM6KGj5pyN5b6I5ryC5Lqu77yM5pyL5Y+L5b6I5Zac5qyi77yM5pyA5Li76KaB55qE5piv5Lmw5a625aSq5aW95LqGfn5+5aSn5aSn55qE6LWe5LiA5Liq44CC44CC44CCIOiho+acje+8jOW+iOWQiOi6q1wiLFxuICAgICAgICBzdGFydDogNCxcbiAgICAgICAgY2hpbGRyZW46IFtdXG4gICAgICB9XG5cblxuICAgIF0sXG4gICAgY29tbWVudExpc3QxOiBbXSxcbiAgfVxuXG4gJHJlcGVhdCA9IHt9O1xyXG4kcHJvcHMgPSB7XCJjb21tZW50TGlzdFwiOntcInYtYmluZDpsaXN0LnN5bmNcIjpcImNvbW1lbnRMaXN0XCJ9LFwidGltZXJcIjp7XCJ4bWxuczp2LWJpbmRcIjpcIlwiLFwidi1iaW5kOnN0YXJ0VGltZS5zeW5jXCI6XCJzdGFydFRpbWVcIixcInYtYmluZDplbmRUaW1lLnN5bmNcIjpcImVuZFRpbWVcIn19O1xyXG4kZXZlbnRzID0ge307XHJcbiBjb21wb25lbnRzID0ge1xuICAgIGNvbW1lbnRMaXN0OiBDb21tZW50TGlzdCxcbiAgICB0aW1lcjp0aW1lclxuICB9XG4gIG9uTG9hZChvcHRpb24pIHtcbiAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgdGhpcy5vcmRlck51bSA9IDE7XG4gICAgdGhpcy5wdXJjaGFzZXR5cGUgPSAxO1xuICAgIHRoaXMuaXNGYXZvcml0ZSA9IGZhbHNlO1xuICAgIHRoaXMuaXNWYWxpZERhdGUgPSB0cnVlO1xuICAgIHRoaXMuY2FuT3JkZXIgPSB0cnVlO1xuICAgIHRoaXMuaGlkZGVuID0gdHJ1ZTtcbiAgICB0aGlzLndpbkhlaWdodCA9IFwiMTAwJVwiO1xuICAgIHRoYXQuZGV0YWlsID0ge307XG4gICAgdGhhdC4kYXBwbHkoKTtcbiAgICAvL+aOpeaUtuS4iuS4gOS4qumhtemdouS8oOi/h+adpeeahOWPguaVsFxuICAgIHRoYXQuZ29vZHNJZCA9IG9wdGlvbi5pZDtcbiAgICBpZiAob3B0aW9uLnB1cmNoYXNldHlwZSAhPSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMucHVyY2hhc2V0eXBlID0gb3B0aW9uLnB1cmNoYXNldHlwZTtcbiAgICB9XG4gICAgaWYgKHRoaXMucHVyY2hhc2V0eXBlID09IDIpIHtcbiAgICAgIHRoaXMucHVyY2hhc2VUZXh0ID0gXCLnlLPor7fooaXotKdcIjtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wdXJjaGFzZVRleHQgPSBcIueri+WNs+i0reS5sFwiO1xuICAgIH1cbiAgICBpZiAob3B0aW9uLnNwZWNpYWwgIT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLnNwZWNpYWwgPSBvcHRpb24uc3BlY2lhbDtcbiAgICB9XG4gICAgdGhhdC5nZXRHb29kc0RldGFpbCgpO1xuICAgIHRoYXQuYWRkVXNlckJyb3dzZXIoKTtcbiAgICBjb25zb2xlLmxvZyhcInNwZWNpYWw9PT1cIiArIHRoaXMuc3BlY2lhbCk7XG4gIH1cbiAgb25TaG93KCkge1xuICAgIHRoaXMuZ29vZHNJc0Zhdm9yaXRlKCk7XG4gICAgLy/liJvlu7rliqjnlLtcbiAgICB2YXIgYW5pbWF0aW9uID0gd3guY3JlYXRlQW5pbWF0aW9uKHtcbiAgICAgIHRyYW5zZm9ybU9yaWdpbjogXCI1MCUgNTAlXCIsXG4gICAgICBkdXJhdGlvbjogMjAwLFxuICAgICAgdGltaW5nRnVuY3Rpb246IFwibGluZWFyXCIsXG4gICAgICBkZWxheTogMFxuICAgIH0pXG4gICAgdGhpcy5hbmltYXRpb24gPSBhbmltYXRpb247XG4gIH1cbiAgd3hQYXJzZUltZ0xvYWQoZSkge31cblxuICB3eFBhcnNlSW1nVGFwKGUpIHtcbiAgICB2YXIgdGhhdCA9IHRoaXNcbiAgICB2YXIgbm93SW1nVXJsID0gZS50YXJnZXQuZGF0YXNldC5zcmNcbiAgICB2YXIgdGFnRnJvbSA9IGUudGFyZ2V0LmRhdGFzZXQuZnJvbVxuICAgIGlmICh0eXBlb2YodGFnRnJvbSkgIT0gJ3VuZGVmaW5lZCcgJiYgdGFnRnJvbS5sZW5ndGggPiAwKSB7XG4gICAgICB3eC5wcmV2aWV3SW1hZ2Uoe1xuICAgICAgICBjdXJyZW50OiBub3dJbWdVcmwsIC8vIOW9k+WJjeaYvuekuuWbvueJh+eahGh0dHDpk77mjqVcbiAgICAgICAgLy8gdXJsczogdGhhdC5kYXRhW3RhZ0Zyb21dLmltYWdlVXJscyAvLyDpnIDopoHpooTop4jnmoTlm77niYdodHRw6ZO+5o6l5YiX6KGoXG4gICAgICAgIHVybHM6IHRoYXQuYmluZERhdGFbdGFnRnJvbV0uaW1hZ2VVcmxzIC8vIOazqOmHiuaOieS4iumdoueahCDmjaLnnYDkuIDooYwgKGh0dHA6Ly9ibG9nLmNzZG4ubmV0L3podW1pbmczODM0L2FydGljbGUvZGV0YWlscy83NDM4MDA3OSlcbiAgICAgIH0pXG4gICAgfVxuICB9XG4gIC8qb25SZWFjaEJvdHRvbSgpIHtcbiAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgaWYgKHRoYXQuZ29vZF9iaWdpbWcubGVuZ3RoID09IDApIHtcbiAgICAgIHRoYXQuZ29vZF9iaWdpbWcgPSB0aGF0LmRldGFpbC5nb29kX2JpZ2ltZztcbiAgICAgIHRoYXQuJGFwcGx5KCk7XG4gICAgfVxuICB9Ki9cbiAgYXN5bmMgZ2V0R29vZHNEZXRhaWwoKSB7XG4gICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgIC8vY29uc3QganNvbiA9IGF3YWl0IGFwaS5nZXRHb29kc0RldGFpbCh7XG4gICAgY29uc3QganNvbiA9IGF3YWl0IGFwaS5nb29kc0RldGFpbCh7XG4gICAgICBxdWVyeToge1xuICAgICAgICBpZDogdGhhdC5nb29kc0lkXG4gICAgICB9XG4gICAgfSk7XG4gICAgbGV0IHRpbWUgPSB7fTtcbiAgICBpZiAoanNvbi5kYXRhLmNvZGUgPT0gMCkge1xuICAgICAgbGV0IGRhdGEgPSBqc29uLmRhdGEuZGF0YTtcbiAgICAgIHRoYXQuZGV0YWlsID0gZGF0YTtcbiAgICAgIFd4UGFyc2Uud3hQYXJzZSgnZGV0YWlsSW5mbycsICdodG1sJywgdGhhdC5kZXRhaWwuZGV0YWlsSW5mbywgdGhpcyk7XG4gICAgICB0aW1lLmVuZFRpbWUgPSB0aGF0LmRldGFpbC52YWxpZEVuZFRpbWU7XG4gICAgICB0aW1lLnN0YXJ0VGltZSA9IHRoYXQuZGV0YWlsLnN0YXJ0VGltZTtcbiAgICAgIGlmIChqc29uLmRhdGEudmFsaWREYXRlID09IFwiMFwiKSB7XG4gICAgICAgIHRoYXQuaXNWYWxpZERhdGUgPSBmYWxzZTtcbiAgICAgICAgaWYgKHRoaXMucHVyY2hhc2V0eXBlID09IDEgJiYgdGhpcy5zcGVjaWFsICE9IDEpIHtcbiAgICAgICAgICB0aGlzLmNhbk9yZGVyID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGpzb24uZGF0YS5tc2cpIHtcbiAgICAgICAgdGlwLmVycm9yKGpzb24uZGF0YS5tc2cpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aXAuZXJyb3IoJ+afpeeci+WVhuWTgeWksei0pScpXG4gICAgICB9XG4gICAgfVxuICAgIHRoYXQuJGFwcGx5KCk7XG4gICAgdGhpcy4kaW52b2tlKCd0aW1lcicsICdpbml0VGltZXInLCB0aW1lKTtcbiAgfVxuICBhc3luYyBhZGRVc2VyQnJvd3NlcigpIHtcbiAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgbGV0IHVzZXJTcGVjaWFsSW5mbyA9IHdlcHkuZ2V0U3RvcmFnZVN5bmMoVVNFUl9TUEVDSUNBTF9JTkZPKSB8fCB7fTtcbiAgICBsZXQgb3BlbklkID0gdXNlclNwZWNpYWxJbmZvLm9wZW5pZDtcbiAgICBjb25zdCBqc29uID0gYXdhaXQgYXBpLmFkZEJyb3dzZXIoe1xuICAgICAgcXVlcnk6IHtcbiAgICAgICAgZ29vZHNJZDogdGhhdC5nb29kc0lkLFxuICAgICAgICBvcGVuSWQ6IG9wZW5JZFxuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIGNvbXB1dGVkID0ge31cbiAgZXZlbnRzID0ge31cbiAgLy/liqDlhaXotK3nianovaZcbiAgYXN5bmMgZG9UYWtlQ2FydCgpIHtcbiAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgbGV0IHVzZXJTcGVjaWFsSW5mbyA9IHdlcHkuZ2V0U3RvcmFnZVN5bmMoVVNFUl9TUEVDSUNBTF9JTkZPKSB8fCB7fTtcbiAgICBsZXQgb3BlbklkID0gdXNlclNwZWNpYWxJbmZvLm9wZW5pZDtcbiAgICBjb25zdCBqc29uID0gYXdhaXQgYXBpLmFkZENhcnQoe1xuICAgICAgcXVlcnk6IHtcbiAgICAgICAgb3BlbklkOiBvcGVuSWQsXG4gICAgICAgIGdvb2RzSWQ6IHRoYXQuZ29vZHNJZCxcbiAgICAgICAgZ29vZHNTa3VJZDogdGhpcy5kZXRhaWwuZ29vZHNTa3VWYWxJZHMsXG4gICAgICAgIHB1cmNoYXNlVHlwZTogdGhpcy5wdXJjaGFzZXR5cGUsXG4gICAgICAgIG51bTogdGhpcy5vcmRlck51bVxuICAgICAgfVxuICAgIH0pO1xuICAgIGlmIChqc29uLmRhdGEuY29kZSA9PSAwKSB7XG4gICAgICB0aGlzLndpbkhlaWdodCA9IFwiMTAwJVwiO1xuICAgICAgdGhpcy5hbmltYXRpb24uaGVpZ2h0KDApLnN0ZXAoKTtcbiAgICAgIHRoaXMuc2V0RGF0YSh7XG4gICAgICAgIGFuaW1hdGlvbkRhdGE6IHRoaXMuYW5pbWF0aW9uLmV4cG9ydCgpXG4gICAgICB9KVxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuaGlkZGVuID0gdHJ1ZTtcbiAgICAgICAgdGhpcy4kYXBwbHkoKTtcbiAgICAgIH0sIDEwMClcbiAgICAgIHRpcC5zdWNjZXNzKFwi5oiQ5Yqf5Yqg5YWl6LSt54mp6L2mXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoanNvbi5kYXRhLm1zZykge1xuICAgICAgICB0aXAuZXJyb3IoanNvbi5kYXRhLm1zZylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRpcC5lcnJvcign5peg5rOV5Yqg5YWl6LSt54mp6L2mJylcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLy/nq4vljbPotK3kubBcbiAgYXN5bmMgZG9UYWtlT3JkZXIoKSB7XG4gICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgIGxldCB1c2VyU3BlY2lhbEluZm8gPSB3ZXB5LmdldFN0b3JhZ2VTeW5jKFVTRVJfU1BFQ0lDQUxfSU5GTykgfHwge307XG4gICAgbGV0IG9wZW5JZCA9IHVzZXJTcGVjaWFsSW5mby5vcGVuaWQ7XG4gICAgY29uc3QganNvbiA9IGF3YWl0IGFwaS5hZGRDYXJ0KHtcbiAgICAgIHF1ZXJ5OiB7XG4gICAgICAgIG9wZW5JZDogb3BlbklkLFxuICAgICAgICBnb29kc0lkOiB0aGF0Lmdvb2RzSWQsXG4gICAgICAgIGdvb2RzU2t1SWQ6IHRoaXMuZGV0YWlsLmdvb2RzU2t1VmFsSWRzLFxuICAgICAgICBwdXJjaGFzZVR5cGU6IHRoaXMucHVyY2hhc2V0eXBlLFxuICAgICAgICBudW06IHRoaXMub3JkZXJOdW1cbiAgICAgIH1cbiAgICB9KTtcbiAgICBpZiAoanNvbi5kYXRhLmNvZGUgPT0gMCkge1xuICAgICAgdGhpcy53aW5IZWlnaHQgPSBcIjEwMCVcIjtcbiAgICAgIHRoaXMuYW5pbWF0aW9uLmhlaWdodCgwKS5zdGVwKCk7XG4gICAgICB0aGlzLnNldERhdGEoe1xuICAgICAgICBhbmltYXRpb25EYXRhOiB0aGlzLmFuaW1hdGlvbi5leHBvcnQoKVxuICAgICAgfSlcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLmhpZGRlbiA9IHRydWU7XG4gICAgICAgIHRoaXMuJGFwcGx5KCk7XG4gICAgICB9LCAxMDApXG4gICAgICB3ZXB5Lm5hdmlnYXRlVG8oe1xuICAgICAgICB1cmw6IFwiL3BhZ2VzL2NvbWZpcmVfb3JkZXI/Z29vZHNJZD1cIiArIHRoYXQuZ29vZHNJZCArIFwiJnB1cmNoYXNldHlwZT1cIiArIHRoYXQucHVyY2hhc2V0eXBlXG4gICAgICB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoanNvbi5kYXRhLm1zZykge1xuICAgICAgICB0aXAuZXJyb3IoanNvbi5kYXRhLm1zZylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRpcC5lcnJvcign5peg5rOV56uL5Yi76LSt5LmwJylcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgYXN5bmMgc2hvd0NvbmZpcm1EYXRhKCkge1xuICAgIHRoaXMuYW5pbWF0aW9uLmhlaWdodCgnNzgzcnB4Jykuc3RlcCgpO1xuICAgIHRoaXMuc2V0RGF0YSh7XG4gICAgICBhbmltYXRpb25EYXRhOiB0aGlzLmFuaW1hdGlvbi5leHBvcnQoKVxuICAgIH0pXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLmhpZGRlbiA9IGZhbHNlO1xuICAgICAgbGV0IHN5c3RlbUluZm8gPSB3ZXB5LmdldFN0b3JhZ2VTeW5jKFNZU1RFTV9JTkZPKTtcbiAgICAgIHRoaXMud2luSGVpZ2h0ID0gc3lzdGVtSW5mby53aW5kb3dIZWlnaHQ7XG4gICAgICB0aGlzLiRhcHBseSgpO1xuICAgIH0sIDEwMClcbiAgfVxuICAvL+afpeeci+WVhuWTgeaUtuiXj+eKtuaAgVxuICBhc3luYyBnb29kc0lzRmF2b3JpdGUoKSB7XG4gICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgIGxldCB1c2VyU3BlY2lhbEluZm8gPSB3ZXB5LmdldFN0b3JhZ2VTeW5jKFVTRVJfU1BFQ0lDQUxfSU5GTykgfHwge307XG4gICAgbGV0IG9wZW5JZCA9IHVzZXJTcGVjaWFsSW5mby5vcGVuaWQ7XG4gICAgY29uc3QganNvbiA9IGF3YWl0IGFwaS5nb29kc0lzRmF2b3JpdGUoe1xuICAgICAgcXVlcnk6IHtcbiAgICAgICAgb3BlbklkOiBvcGVuSWQsXG4gICAgICAgIGdvb2RzSWQ6IHRoYXQuZ29vZHNJZFxuICAgICAgfVxuICAgIH0pO1xuICAgIGlmIChqc29uLmRhdGEuY29kZSA9PSAwKSB7XG4gICAgICBpZiAoanNvbi5kYXRhLmlzRmF2b3JpdGUgPT0gMSkge1xuICAgICAgICB0aGlzLmlzRmF2b3JpdGUgPSB0cnVlO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmlzRmF2b3JpdGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5pc0Zhdm9yaXRlID0gZmFsc2U7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUubG9nKCfmn6XnnIvllYblk4HmlLbol4/lpLHotKUnKVxuICAgICAgaWYgKGpzb24uZGF0YS5tc2cpIHtcbiAgICAgICAgdGlwLmVycm9yKGpzb24uZGF0YS5tc2cpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aXAuZXJyb3IoJ+afpeeci+WVhuWTgeaUtuiXj+Wksei0pScpXG4gICAgICB9XG4gICAgfVxuICAgIHRoYXQuJGFwcGx5KCk7XG4gIH1cbiAgLy/llYblk4HmlLbol49cbiAgYXN5bmMgZ29vZHNGYXZvcml0ZSgpIHtcbiAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgbGV0IHVzZXJTcGVjaWFsSW5mbyA9IHdlcHkuZ2V0U3RvcmFnZVN5bmMoVVNFUl9TUEVDSUNBTF9JTkZPKSB8fCB7fTtcbiAgICBsZXQgb3BlbklkID0gdXNlclNwZWNpYWxJbmZvLm9wZW5pZDtcbiAgICBjb25zdCBqc29uID0gYXdhaXQgYXBpLmdvb2RzRmF2b3JpdGUoe1xuICAgICAgcXVlcnk6IHtcbiAgICAgICAgb3BlbklkOiBvcGVuSWQsXG4gICAgICAgIGdvb2RzSWQ6IHRoYXQuZ29vZHNJZFxuICAgICAgfVxuICAgIH0pO1xuICAgIGlmIChqc29uLmRhdGEuY29kZSA9PSAwKSB7XG4gICAgICBjb25zb2xlLmxvZyhcIj09PT09PT09PT095ZWG5ZOB5pS26JeP5oiQ5YqfPT09PT09PT09XCIpXG4gICAgICB0aGlzLmlzRmF2b3JpdGUgPSB0cnVlO1xuICAgICAgdGlwLnRvYXN0KFwi5pS26JeP5oiQ5YqfXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhqc29uLmRhdGEpXG4gICAgICB0aXAuZXJyb3IoJ+aUtuiXj+Wksei0pScpXG4gICAgfVxuICAgIHRoYXQuJGFwcGx5KCk7XG4gIH1cbiAgLy/llYblk4Hlj5bmtojmlLbol49cbiAgYXN5bmMgZ29vZHNVbkZhdm9yaXRlKCkge1xuICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICBsZXQgdXNlclNwZWNpYWxJbmZvID0gd2VweS5nZXRTdG9yYWdlU3luYyhVU0VSX1NQRUNJQ0FMX0lORk8pIHx8IHt9O1xuICAgIGxldCBvcGVuSWQgPSB1c2VyU3BlY2lhbEluZm8ub3BlbmlkO1xuICAgIGNvbnN0IGpzb24gPSBhd2FpdCBhcGkuZ29vZHNVbkZhdm9yaXRlKHtcbiAgICAgIHF1ZXJ5OiB7XG4gICAgICAgIG9wZW5JZDogb3BlbklkLFxuICAgICAgICBnb29kc0lkOiB0aGF0Lmdvb2RzSWRcbiAgICAgIH1cbiAgICB9KTtcbiAgICBpZiAoanNvbi5kYXRhLmNvZGUgPT0gMCkge1xuICAgICAgY29uc29sZS5sb2coXCI9PT09PT09PT09PeWVhuWTgeWPlua2iOaUtuiXj+aIkOWKnz09PT09PT09PVwiKVxuICAgICAgdGlwLnRvYXN0KFwi5Y+W5raI5pS26JeP5oiQ5YqfXCIpO1xuICAgICAgdGhpcy5pc0Zhdm9yaXRlID0gZmFsc2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUubG9nKGpzb24uZGF0YSlcbiAgICAgIHRpcC5lcnJvcign5Y+W5raI5pS26JeP5aSx6LSlJylcbiAgICB9XG4gICAgdGhhdC4kYXBwbHkoKTtcbiAgfVxuICBtZXRob2RzID0ge1xuICAgIC8vIGlzc3VzIDogaHR0cHM6Ly9tcC53ZWl4aW4ucXEuY29tL2RlYnVnL3d4YWRvYy9kZXYvYXBpL3VpLW5hdmlnYXRlLmh0bWwjd3hyZWxhdW5jaG9iamVjdFxuICAgIGhvbWVQYWdlKCkge1xuICAgICAgd2VweS5zd2l0Y2hUYWIoe1xuICAgICAgICB1cmw6ICcvcGFnZXMvaG9tZSdcbiAgICAgIH0pXG4gICAgICAvLyB3eC5zd2l0Y2hUYWIoe1xuICAgICAgLy8gICB1cmw6ICcvcGFnZXMvaG9tZSdcbiAgICAgIC8vIH0pXG4gICAgICAvLyBjb25zb2xlLmxvZyh3ZXB5KVxuICAgICAgLy8gY29uc29sZS5sb2cod3gpXG4gICAgfSxcbiAgICBtb3JlQ29tbWVudCgpIHtcbiAgICAgIHdlcHkubmF2aWdhdGVUbyh7XG4gICAgICAgIHVybDogXCIvcGFnZXMvY29tbWVudFwiXG4gICAgICB9KVxuICAgIH0sXG4gICAgICAvL2xldCBjdXJyZW50ID0gZS50YXJnZXQuZGF0YXNldC5zcmM7XG4gICAgcHJldmlld0ltYWdlKGUpIHtcbiAgICAgICAgICBsZXQgY3VycmVudCA9IGUudGFyZ2V0LmRhdGFzZXQuc3JjO1xuXG4gICAgICBsZXQgaW1hZ2VBcnJ5ID0gW107XG4gICAgICBsZXQgb2JqID0gdGhpcy5kZXRhaWwucGhvdG9MaXN0O1xuICAgICAgT2JqZWN0LmtleXMob2JqKS5mb3JFYWNoKChpdGVtKSA9PiB7XG4gICAgICAgIGltYWdlQXJyeS5wdXNoKG9ialtpdGVtXS5waG90bylcbiAgICAgIH0pO1xuICAgICAgd3gucHJldmlld0ltYWdlKHtcbiAgICAgICAgY3VycmVudDogY3VycmVudCwgLy8g5b2T5YmN5pi+56S65Zu+54mH55qEaHR0cOmTvuaOpVxuICAgICAgICB1cmxzOiBpbWFnZUFycnksIC8vIOmcgOimgemihOiniOeahOWbvueJh2h0dHDpk77mjqXliJfooahcbiAgICAgIH0pXG4gICAgfSxcbiAgICBiaW5kT3JkZXJOdW1JbnB1dChlKSB7XG4gICAgICB0aGlzLm9yZGVyTnVtID0gZS5kZXRhaWwudmFsdWU7XG4gICAgfSxcbiAgICB0YWtlT3JkZXIoKSB7XG4gICAgICBpZiAoIXRoaXMuY2FuT3JkZXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5zaG93Q29uZmlybURhdGEoKTtcbiAgICAgIHRoaXMub3JkZXJUeXBlID0gMjtcbiAgICAgIC8vdGhpcy5kb1Rha2VPcmRlcigpO1xuICAgIH0sXG4gICAgdGFrZUNhcnQoKSB7XG4gICAgICBpZiAoIXRoaXMuY2FuT3JkZXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5zaG93Q29uZmlybURhdGEoKTtcbiAgICAgIHRoaXMub3JkZXJUeXBlID0gMTtcbiAgICAgIC8vdGhpcy5kb1Rha2VDYXJ0KCk7XG4gICAgfSxcbiAgICB0YWtlRmF2b3JpdGUoKSB7XG4gICAgICBpZiAodGhpcy5pc0Zhdm9yaXRlID09IHRydWUpIHtcbiAgICAgICAgdGhpcy5nb29kc1VuRmF2b3JpdGUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZ29vZHNGYXZvcml0ZSgpO1xuICAgICAgfVxuICAgIH0sXG4gICAgY2xvc2VNb2RlbCgpIHtcbiAgICAgIHRoaXMud2luSGVpZ2h0ID0gXCIxMDAlXCI7XG4gICAgICB0aGlzLmFuaW1hdGlvbi5oZWlnaHQoMCkuc3RlcCgpO1xuICAgICAgdGhpcy5zZXREYXRhKHtcbiAgICAgICAgYW5pbWF0aW9uRGF0YTogdGhpcy5hbmltYXRpb24uZXhwb3J0KClcbiAgICAgIH0pXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgdGhpcy5oaWRkZW4gPSB0cnVlO1xuICAgICAgICB0aGlzLiRhcHBseSgpO1xuICAgICAgfSwgMTAwKVxuICAgIH0sXG4gICAgY29uZmlybVRha2UoKSB7IC8v56Gu5a6a6LSt54mp6L2m5oiW6ICF55u05o6l6LSt5LmwXG4gICAgICBpZiAodGhpcy5vcmRlclR5cGUgPT0gMSkge1xuICAgICAgICB0aGlzLmRvVGFrZUNhcnQoKTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5vcmRlclR5cGUgPT0gMikge1xuICAgICAgICB0aGlzLmRvVGFrZU9yZGVyKCk7XG4gICAgICB9XG4gICAgfSxcbiAgICBqaWFCdG5UYXAoZSkge1xuICAgICAgdGhpcy5vcmRlck51bSsrO1xuICAgIH0sXG4gICAgamlhbkJ0blRhcCgpIHtcbiAgICAgIGlmICh0aGlzLm9yZGVyTnVtID4gMSkge1xuICAgICAgICB0aGlzLm9yZGVyTnVtLS07XG4gICAgICB9XG4gICAgfSxcbiAgICBzZWxBdHRyKGUpIHtcbiAgICAgIHZhciBpZCA9IGUuY3VycmVudFRhcmdldC5kYXRhc2V0LmlkO1xuICAgICAgdmFyIG5hbWVpZCA9IGUuY3VycmVudFRhcmdldC5kYXRhc2V0Lm5hbWVpZDtcbiAgICAgIHZhciBpbmRleCA9IGUuY3VycmVudFRhcmdldC5kYXRhc2V0LmluZGV4O1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmRldGFpbC5nb29kc1NrdU5hbWVMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBza3VWYWxMaXN0ID0gdGhpcy5kZXRhaWwuZ29vZHNTa3VOYW1lTGlzdFtpXS5za3VWYWxMaXN0O1xuICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHNrdVZhbExpc3QubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICB2YXIgc2t1VmFsID0gc2t1VmFsTGlzdFtqXTtcbiAgICAgICAgICAgIGlmIChza3VWYWwuc2t1VmFsSWQgPT0gaWQpIHtcbiAgICAgICAgICBpZiAoc2t1VmFsLnNrdU5hbWVJZCA9PSBuYW1laWQpIHtcbiAgICAgICAgICAgIHNrdVZhbC5jdXJyZW50ID0gZmFsc2U7XG4gICAgICAgICAgICAgIHNrdVZhbC5jdXJyZW50ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgdGhpcy5kZXRhaWwuZ29vZHNTa3VWYWxJZHNbaW5kZXhdID0gaWQ7XG4gICAgICAgICAgICAgIGZvciAodmFyIGsgPSAwOyBrIDwgdGhpcy5kZXRhaWwuZ29vZHNTa3VMaXN0Lmxlbmd0aDsgaysrKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNrdVZhbElkcyA9IEpTT04ucGFyc2UodGhpcy5kZXRhaWwuZ29vZHNTa3VMaXN0W2tdLnNrdVZhbElkcykudG9BcnJheTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImdvb2Rza3VpZHMuLi5cIiArIHRoaXMuZGV0YWlsLmdvb2RzU2t1TGlzdFtrXS5za3VWYWxJZHMpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidGhpcyBnb29kc2t1aWRzLi4uXCIgKyB0aGlzLmRldGFpbC5nb29kc1NrdVZhbElkcyk7XG4gICAgICAgICAgICAgICAgaWYgKFwiW1wiICsgdGhpcy5kZXRhaWwuZ29vZHNTa3VWYWxJZHMudG9TdHJpbmcoKSArIFwiXVwiID09PSB0aGlzLmRldGFpbC5nb29kc1NrdUxpc3Rba10uc2t1VmFsSWRzKSB7XG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImdvb2Rza3VpZHMgZXF1YWxzLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgdGhpcy5kZXRhaWwuc3RvY2tOdW0gPSB0aGlzLmRldGFpbC5nb29kc1NrdUxpc3Rba10uc3RvY2tOdW07XG4gICAgICAgICAgICAgICAgICB0aGlzLmRldGFpbC5wcmljZSA9IHRoaXMuZGV0YWlsLmdvb2RzU2t1TGlzdFtrXS5wcmljZTtcbiAgICAgICAgICAgICAgICAgIHRoaXMuJGFwcGx5KCk7XG4gICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuICAgIG9uU2hhcmVBcHBNZXNzYWdlOiBmdW5jdGlvbihyZXMpIHtcbiAgICAgIGlmIChyZXMuZnJvbSA9PT0gJ2J1dHRvbicpIHtcbiAgICAgICAgLy8g5p2l6Ieq6aG16Z2i5YaF6L2s5Y+R5oyJ6ZKuXG4gICAgICAgIGNvbnNvbGUubG9nKHJlcy50YXJnZXQpXG4gICAgICB9XG4gICAgICByZXR1cm4ge1xuICAgICAgICB0aXRsZTogdGhpcy5kZXRhaWwubmFtZSxcbiAgICAgICAgcGF0aDogJy9wYWdlcy9nb29kc19kZXRhaWw/aWQ9JyArIHRoaXMuZ29vZHNJZCxcbiAgICAgICAgc3VjY2VzczogZnVuY3Rpb24ocmVzKSB7XG4gICAgICAgICAgLy8g6L2s5Y+R5oiQ5YqfXG4gICAgICAgIH0sXG4gICAgICAgIGZhaWw6IGZ1bmN0aW9uKHJlcykge1xuICAgICAgICAgIC8vIOi9rOWPkeWksei0pVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbiJdfQ==